const express = require("express");
const httpsService = express();
const { MiddleWares } = require("./middlewares");
exports.HTTP = class extends MiddleWares {
    constructor() {
        super();
    }
    static start() {
        MiddleWares.execute(httpsService);
        this.port = 9999;
        if (this.port) {
            httpsService.listen(this.port, () => {
                console.log(`Server started at port ${this.port}`);
            });
        } else {
            console.log("NO PORT");
        }
    }
};