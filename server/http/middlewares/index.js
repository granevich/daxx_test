const router = require("../router");
const morgan = require("morgan");
exports.MiddleWares = class {
    constructor() {}
    static execute(app) {
        app.use(morgan("dev"));
        app.use("/v1/", router);
    }

};
