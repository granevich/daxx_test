exports.HTTPController = class {
    constructor() {}
    static CreateUser(req, res){
        require('./modules/CreateUser')(req, res);
    }
    static ListUsers(req, res){
        require('./modules/ListUsers')(req, res);
    }
    static GetUser(req, res){
        require('./modules/GetUser')(req, res);
    }
    static UpdateUser(req, res){
        require('./modules/UpdateUser')(req, res);
    }
    static DeleteUser(req, res){
        require('./modules/DeleteUser')(req, res);
    }

};
