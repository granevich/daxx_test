const { UserDBController } = require('../../../database/controllers/UserController');

module.exports = async (req, res) => {
    try {
      let st = await UserDBController.DeleteUser(req.query);
      if (st) res.status(200).send({code:200, st});
    } catch (e) {
        console.error(e.message || e);
        const err = {
            error: {
                code:500,
                message:e.message,
                status:"INTERNAL SERVER ERROR",
                details:[
                    {
                        message: e.message
                    }
                ]
            }

        };
        res.status(500).send(JSON.stringify(err));
    }
};