const { UserDBController } = require('../../../database/controllers/UserController');
const doCheck = require('../helpers');
module.exports = async (req, res) => {
    try {
        const status =  doCheck(req.query);
        let stat;
        if(status){
           stat = await UserDBController.GetUser(req.query);
        }else{
            res.status(200).send({code:404, message:"One of Query Params Is Wrong"});
        }
        if(stat.success) res.status(200).send({code:200, stat});
    } catch (e) {
        console.error(e.message || e);
        const err = {
            error: {
                code:500,
                message:e.message,
                status:"INTERNAL SERVER ERROR",
                details:[
                    {
                        message: e.message
                    }
                ]
            }

        };
        res.status(500).send(JSON.stringify(err));
    }
};