const { UserDBController } = require('../../../database/controllers/UserController');

module.exports = async (req, res) => {
    try {
        const stat = await UserDBController.ListUsers();
        if(stat.success) res.status(200).send({code:200, stat, message:"User List Obtained Successfully"});
    } catch (e) {
        console.error(e.message || e);
        const err = {
            error: {
                code:500,
                message:e.message,
                status:"INTERNAL SERVER ERROR",
                details:[
                    {
                        message: e.message
                    }
                ]
            }

        };
        res.status(500).send(JSON.stringify(err));
    }
};