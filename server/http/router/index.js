const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const JsonParser = bodyParser.json();
const { HTTPController } = require("../controllers");

class Router {
    constructor() {}
    static routing() {
        return [
            router.post("/user", JsonParser, HTTPController.CreateUser),
            router.get("/users", HTTPController.ListUsers),
            router.get("/user:search", HTTPController.GetUser),
            router.patch("/user", JsonParser, HTTPController.UpdateUser),
            router.delete("/user", HTTPController.DeleteUser),
        ];
    }
}

module.exports = Router.routing();