const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const validator = require('validator');
mongoose.Promise = global.Promise;

const userSchema = new Schema({
    email:{
        type: String,
        unique: true,
        lowercase: true,
        trim: true,
        validate: [validator.isEmail, "Invalid Email Address"],
        required: true
    },
    password:{
        type:String,
        required:true,
        validate: function (password) {
            return /^(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d]{8,}$/.test(password);
        }

    },
    firstName:{
        type: String,
        required: true,
        max: 25
    },
    lastName:{
        type: String,
        required: true,
        max: 25
    },
    city:{
        type:String,
        max:25,
        required:false
    }
});

module.exports = mongoose.model('User', userSchema);