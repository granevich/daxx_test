const UserModel = require("../schema/User");

exports.UserDBController = class {
  constructor() {}
  static CreateUser(user) {
    const { email, password, firstName, lastName, city } = user;
    return new Promise((resolve, reject) => {
      (async () => {
        try {
          let userS = new UserModel({
            email,
            password,
            firstName,
            lastName,
            city
          });
          await userS.save();
          resolve({ success: true });
        } catch (e) {
          reject(e);
        }
      })();
    });
  }
  static ListUsers() {
    return new Promise((resolve, reject) => {
      (async () => {
        try {
          const payload = await UserModel.find();
          resolve({ success: true, payload });
        } catch (e) {
          console.error(e);
          reject(e);
        }
      })();
    });
  }
  static GetUser(query) {
    return new Promise((resolve, reject) => {
      (async () => {
        try {
          let q = {};
          for (let key in query) {
            let regex = new RegExp(query[key]);
            q[key] = {
              $regex: regex,
              $options: "i"
            };
          }
          const payload = await UserModel.find(q);
          // const payload = await UserModel.find({"email": {$regex: /nn.cc/, $options:'i'}});
          payload.length !== 0
            ? resolve({
                success: true,
                payload,
                message: "User List Obtained Successfully"
              })
            : resolve({ success: true, payload, message: "No Data" });
        } catch (e) {
          console.error(e);
          reject(e);
        }
      })();
    });
  }

  static UpdateUser(req){
    return new Promise((resolve, reject) => {
      (async () => {
        try {


         let st =  await UserModel.findByIdAndUpdate(req._id, req.data);
         if(st) resolve ({
                success: true,
                message: "User Updated Successfully"
          })
        } catch (e) {
          reject(e);
        }
      })();
    });
  }
  static DeleteUser({_id}){
    return new Promise((resolve, reject) => {
      (async () => {
        try {
         let st =  await UserModel.findByIdAndDelete({_id});
         if(st){ resolve ({
                success: true,
                message: "User Deleted Successfully"
          }) }else{
           resolve({success:false, message:" There is no user with the such ID"})
         }
        } catch (e) {
          reject(e);
        }
      })();
    });
  }
};
