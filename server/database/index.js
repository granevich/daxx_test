const mongoose = require("mongoose");
const path = require("path");
const fs = require("fs");
exports.Database = class {
  constructor() {}
  static start() {
    const user = "user";
    const password = "VLb4VsfPVEcU5bQa";
    fs.readFile(path.resolve(__dirname, './config/config.json'), "utf-8", (err, data) => {
      if (err) throw new Error(err);
      this.config = JSON.parse(data);
    });
    mongoose.connect(`mongodb://${user}:${password}@ds235078.mlab.com:35078/daxx_test`, this.config);
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    mongoose.set('useUnifiedTopology', true);

    mongoose.connection.on("connected", () => {
      console.log("Database Connection: Success");
    });
    mongoose.connection.on("error", err => {
      console.log(`Database error ${err}`);
    });
  }
};
