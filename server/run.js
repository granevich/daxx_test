exports.Server = class {
    constructor() {}
    static run(...services) {
        services.forEach(s => {
            s.start();
        });
    }
};
