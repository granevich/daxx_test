const { Server } = require("./server/run");
const { Database } = require("./server/database");
const { HTTP } = require("./server/http");
Server.run(HTTP, Database);