## Daxx Test Tast 

First of all run `npm i`

To run the application please type:

`npm start`

To get the rest API Documentation please fallow the [link](https://app.swaggerhub.com/apis/granevich/Daxx_Users_api/0.0.1 "link")

## The main API endpoints: 
#### Server Domain should be:

`http://127.0.0.1:9999/v1/`

#### List ALL Users:

`GET /users`

#### Add new User 

`POST /user`

#### Update User 

`PATCH /user`

#### Delete User 

`DELETE /user`

#### Filter User by conditions

`GET /use:search`

For more detail information please use [API Documentation](https://app.swaggerhub.com/apis/granevich/Daxx_Users_api/0.0.1 "API Documentation") or check the sourse code. 